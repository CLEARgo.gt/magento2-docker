Copy and modified from https://hub.docker.com/r/rafaelcgstz/magento2/. 

How to use:
-----------
1. Build the php version you want. e.g. `./build php70`
2. Copy project (template) to new place. <new-project-path>
3. Update <new-project-path>/docker-compose.yml apache.image if necessary. lamp:7.0-apache, lamp:7.1-apache etc.
4. <new-project-path>/./start
5. with `docker-compose ps` you should see all service up. 