# Alias

alias n98='n98-magerun2.phar'
alias magerun='n98-magerun2.phar'
alias mage='php -d memory_limit=-1 -f bin/magento'
alias magento='php -d memory_limit=-1 -f bin/magento'
alias node='nodejs'

# NVM

export NVM_DIR="/var/www/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

# xdebug for cli
 export XDEBUG_CONFIG="remote_enable=1 remote_mode=req remote_port=9000 remote_host=172.17.0.1 remote_connect_back=0"